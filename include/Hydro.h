#ifndef HYDRO
#define HYDRO

#include <iostream>
#include <dlib/geometry.h>
#include <dlib/matrix.h>
#include <random>

class Hydro
{

public:
    Hydro(const double d,  //< particle diameter [mu m]
          const double mu, //< Magnetic permeability [N/A^2]
          const double nu, //< viscosoty [N.s/m^2]
          const double kb, //< Boltzmann constant [J⋅K−1]
          const double T   //< Temperature [K]
    );

    dlib::vector<double, 3> lubricantForce(const dlib::vector<double, 3> &relPosition,//< relative position of particle j wrt i
                                              const dlib::vector<double, 3> &relVelocity //< relative velocity of particle j wrt i
    ) const;

    dlib::vector<double, 3> brownianDiffusion(const double dt //< delta time [s]
    );

    double getDrag() const;

    ~Hydro();

private:
    const double _d; //< particle diameter [mu m]
    const double _D; //< Drag coefficient [kg/s]
    const double _c; //< lubricant factor [kg.m/s]
    const double _bf; //< brownian factor 

    std::default_random_engine _gen;
    std::uniform_real_distribution<double> _dis;
    std::uniform_real_distribution<double> _costheta;
    std::uniform_real_distribution<double> _phi;
};
#endif

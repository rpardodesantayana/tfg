#ifndef MAGNETICFIELD
#define MAGNETICFIELD

#include <iostream>
#include <dlib/geometry.h>
#include <dlib/matrix.h>

class MagneticField
{

public:
    MagneticField(const double Rout,  //< Rout Cyl [mu m]
                  const double Rin,   //< Rin Cyl [mu m]
                  const double lx,    //< X separation Cyl [mu m]
                  const double ly,    //< y separation Cyl [mu m]
                  const double h,     //< H Cyl[A/m]
                  const double Hbias, //< Bias Magnetic field[A/m]
                  const double Ms,    //< Cylinder Magnetisation Saturation[A/m]
                  const double Mp,    //< Particle Magnetisation Saturation[A/m]
                  const double mu,    //< Magnetic permeability [N/A^2]
                  const double V      //< Saturation Magnetisation [m^3]
    );

    dlib::vector<double, 3> effMagMoment(const dlib::vector<double, 3> &position);

    dlib::vector<double, 3> dipoleForce(const dlib::vector<double, 3> &mi, //< effective magnetic moment particle i
                                        const dlib::vector<double, 3> &mj,//< effective magnetic moment particle j
                                        const dlib::vector<double, 3> &relPosition //< relative position of particle j wrt i
                                        ) const;

    dlib::vector<double, 3> magneticForce(const dlib::vector<double, 3> &position);

    ~MagneticField();

    static double _R;    //< R Cyl [mu m]
    static double _r;    //< Cyl coord r [mu m]
    static double _z;    //< Cyl coord z [mu m]
    static double _dz;   //< z - z'[mu m]
    static double _f;    //< aux var
    static double _d;    //< aux var
    static double _Rt;   //< aux var
    static double _Zt;   //< aux var
    static double _dRtr; //< aux var
    static double _dZtr; //< aux var
    static double _dRtz; //< aux var
    static double _dZtz; //< aux var
    static double _k;    //< aux var
    static double _dkr;  //< aux var
    static double _dkz;  //< aux var
    static double _E;    //< aux var
    static double _dE;   //< aux var
    static double _K;    //< aux var
    static double _dK;   //< aux var
    static double _tol;  //< Integratrion tolerance. recomended [1e-10, 1e-8]

private:
    const double _Rout;  //< Rout Cyl [mu m]
    const double _Rin;   //< Rin Cyl [mu m]
    const double _lx;    //< X separation Cyl [mu m]
    const double _ly;    //< y separation Cyl [mu m]
    const double _h;     //< H Cyl [mu m]
    const double _Hbias; //< Bias Magnetic field [T]
    const double _Ms;    //< Cylinder Magnetisation Saturation[A/m]
    const double _Mp;    //< Particle Magnetisation Saturation[A/m]
    const double _mu;    //< Magnetic permeability [N/A^2]
    const double _V;     //< Saturation Magnetisation [m^3]
    dlib::matrix<double, 9, 3> _offset;

    static double Pirr(const double zp);
    static double Pirz(const double zp);
    static double Pizr(const double zp);
    static double Pizz(const double zp);
    static double Pir(const double zp);
    static double Piz(const double zp);
    static double K();
    static double dK();
    static double fk(const double phi);
    static double dfk(const double phi);
    static double E();
    static double dE();
    static double fe(const double phi);
    static double dfe(const double phi);
    static double d();
    static double f();
    static double k();
    static double dkr();
    static double dkz();
    static double Rt();
    static double Zt();
    static double dRtr();
    static double dRtz();
    static double dZtr();
    static double dZtz();
    static double dz(const double zp);

    dlib::vector<double, 2> cylField(const double R, const dlib::vector<double, 3> &position);
    dlib::matrix<double, 2, 2> cylFieldGradient(const double R, const dlib::vector<double, 3> &position);
    dlib::vector<double, 3> toCartesian(const dlib::vector<double, 2> &vCyl, dlib::vector<double, 2> &ur) const;
};
#endif

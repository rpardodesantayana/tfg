#ifndef SURFACTANT
#define SURFACTANT

#include <iostream>
#include <dlib/geometry.h>
#include <dlib/matrix.h>

class Surfactant
{

public:
    Surfactant( const double Rp, //< particle radius [mu m]
                const double Ns, //< surface density of surfactant molecules [1/m^2]
                const double kb, //< Boltzmann constant [J⋅K−1]
                const double T,  //< Temperature [K]
                const double d   //< thickness of the surfactant layer [mu m]
    );

    dlib::vector<double, 3> force(const dlib::vector<double, 3> &relPosition //< relative position of particle j wrt i
    ) const;

    dlib::vector<double, 3> force2(const dlib::vector<double, 3> &relPosition //< relative position of particle j wrt i
    ) const;

    ~Surfactant();

private:
    const double _Rp; //< particle radius [mu m]
    const double _d;  //< thickness of the surfactant layer [mu m]
    const double _c;  //< surfactant factor [N]
    const double _c2;  //< surfactant factor [N]
    const double _K;  //< surfactant factor [N]
};
#endif

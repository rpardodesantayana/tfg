#ifndef VDW
#define VDW

#include <iostream>
#include <dlib/geometry.h>
#include <dlib/matrix.h>

class VanDerWaals
{

public:
    VanDerWaals(const double Rp, //< particle radius [mu m]
                const double A   //< Hamaker constant
    );

    dlib::vector<double, 3> force(const dlib::vector<double, 3> &relPosition //< relative position of particle j wrt i
    ) const;

    ~VanDerWaals();

private:
    const double _Rp; //< particle radius [mu m]
    const double _A;  //< Hamaker constant
};
#endif

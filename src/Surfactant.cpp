#include "Surfactant.h"
#include <cmath>
#include <dlib/numerical_integration.h>
#include <dlib/numeric_constants.h>

Surfactant::Surfactant(const double Rp,
                       const double Ns,
                       const double kb,
                       const double T,
                       const double d)
    : _Rp(Rp),
      _d(d),
      _c(2 * dlib::pi * _Rp * _Rp * Ns * kb * T * 1e-6 / _d),
      _c2(2 * dlib::pi *6.8e5),
      _K(2.5e9)
{
}

dlib::vector<double, 3> Surfactant::force(const dlib::vector<double, 3> &relPosition) const
{
    const dlib::vector<double, 3> pos = relPosition * 1e-6;
    double r = std::sqrt(pos.dot(pos));

    double Fij(-_c * std::log(2. * (_Rp + _d) / r));

    if (r > 2. * (_Rp + _d)* 1e-6)
        return 0. * pos;

    return Fij * pos / r;
}

dlib::vector<double, 3> Surfactant::force2(const dlib::vector<double, 3> &relPosition) const
{
    const dlib::vector<double, 3> pos = relPosition * 1e-6;
    double r = std::sqrt(pos.dot(pos)) ;
    double d = 2. * _Rp * 1e-6;
    double h = r - d;

    double Fij(-_c2 * 4.* _Rp* 1e-6 * h/(std::exp(h*_K)+std::exp(-h*_K)-2.));

    if (h<=0.)return 0*pos;

    return Fij * pos / r;
}

Surfactant::~Surfactant()
{
}
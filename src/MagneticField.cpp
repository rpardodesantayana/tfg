#include "MagneticField.h"
#include <cmath>
#include <dlib/numerical_integration.h>
#include <dlib/numeric_constants.h>

MagneticField::MagneticField(const double Rout,
                             const double Rin,
                             const double lx,
                             const double ly,
                             const double h,
                             const double Hbias,
                             const double Ms,
                             const double Mp,
                             const double mu,
                             const double V)
    : _Rout(Rout),
      _Rin(Rin),
      _lx(lx),
      _ly(ly),
      _h(h),
      _Hbias(Hbias),
      _Ms(Ms),
      _Mp(Mp),
      _mu(mu),
      _V(V),
      _offset()
{
    _offset = -1.0, -1.0, 0.0,
    -1.0, 0.0, 0.0,
    -1.0, 1.0, 0.0,
    0.0, -1.0, 0.0,
    0.0, 0.0, 0.0,
    0.0, 1.0, 0.0,
    1.0, -1.0, 0.0,
    1.0, 0.0, 0.0,
    1.0, 1.0, 0.0;
    set_colm(_offset, 0) = _lx * colm(_offset, 0);
    set_colm(_offset, 1) = _ly * colm(_offset, 1);
}

dlib::vector<double, 3> MagneticField::dipoleForce(const dlib::vector<double, 3> &mi,
                                                   const dlib::vector<double, 3> &mj,
                                                   const dlib::vector<double, 3> &relPosition) const
{
    const dlib::vector<double, 3> pos = relPosition * 1e-6;
    double r = std::sqrt(pos.dot(pos));
    double gamma = 0.;
    if (pos.y() * pos.y() + pos.x() * pos.x() >= _tol)
        gamma = std::atan2(pos.y(), pos.x());
    const dlib::vector<double, 3> ur = pos / r;
    const dlib::vector<double, 3> ug = {-std::sin(gamma), std::cos(gamma), 0.};
    const dlib::vector<double, 3> ut = ug.cross(ur);

    double Frij(_mu / 4. / dlib::pi *
                (9. * mi.dot(pos) * mj.dot(pos) / std::pow(r, 6) -
                 3. * mi.dot(mj) / std::pow(r, 4)));
    double Ftij(-_mu / 4. / dlib::pi *
                3. * (mi.dot(pos) * mj.dot(ut) + mj.dot(pos) * mi.dot(ut)) / std::pow(r, 5));
    double Fgij(0.);
    if (gamma != 0.)
        Fgij = _mu / 4. / dlib::pi *
               3. * (mi.dot(pos) * mj.dot(ug) + mj.dot(pos) * mi.dot(ug)) / std::pow(r, 5) / ut.z();

    return Frij * ur + Ftij * ut + Fgij * ug;
}

dlib::vector<double, 3> MagneticField::effMagMoment(const dlib::vector<double, 3> &position)
{
    dlib::vector<double, 3> Ht;
    dlib::vector<double, 2> Hout;
    dlib::vector<double, 2> Hin;

    // for (size_t i = 0; i < _offset.nr(); i++)
    // {
        size_t i(4);
        dlib::matrix<double> offset = rowm(_offset, i);
        dlib::vector<double> vOff(offset);
        dlib::vector<double> newPos(position + vOff);
        dlib::vector<double, 2> ur(1., 0.);
        Hout = cylField(_Rout, newPos);
        Hin = cylField(_Rin, newPos);
        _r = std::sqrt(newPos.x() * newPos.x() + newPos.y() * newPos.y());
        if (_r >= _tol)
            ur = dlib::vector<double, 2>(newPos.x() / _r, newPos.y() / _r);
        Ht += toCartesian(Hout - Hin, ur);
    // }

    dlib::vector<double, 3> Hbias;
    Hbias.z() = _Hbias;
    Ht += Hbias;
    double H(std::sqrt(Ht.dot(Ht)));
    return _V * _Mp / H * Ht;
}

dlib::vector<double, 3> MagneticField::magneticForce(const dlib::vector<double, 3> &position)
{

    dlib::vector<double, 3> F;
    dlib::vector<double, 2> Fcyl;
    dlib::vector<double, 2> Ht;
    dlib::vector<double, 3> HtCart;
    dlib::vector<double, 2> Hout;
    dlib::vector<double, 2> Hin;
    dlib::matrix<double, 2, 2> dHt;
    dlib::matrix<double, 2, 2> dHout;
    dlib::matrix<double, 2, 2> dHin;

    // for (size_t i = 0; i < _offset.nr(); i++)
    // {
        size_t i(4);
        dlib::matrix<double> offset = rowm(_offset, i);
        dlib::vector<double> vOff(offset);
        dlib::vector<double> newPos(position + vOff);
        dlib::vector<double, 2> ur(1., 0.);
        Hout = cylField(_Rout, newPos);
        Hin = cylField(_Rin, newPos);
        Ht = Hout - Hin;
        dHout = cylFieldGradient(_Rout, newPos);
        dHin = cylFieldGradient(_Rin, newPos);
        dHt = dHout - dHin;
        _r = std::sqrt(newPos.x() * newPos.x() + newPos.y() * newPos.y());
        if (_r >= _tol)
            ur = dlib::vector<double, 2>(newPos.x() / _r, newPos.y() / _r);

        HtCart += toCartesian(Ht, ur);
        Fcyl(0) = Ht.x() * dHt(0, 0) + Ht.y() * dHt(0, 1);
        Fcyl(1) = Ht.x() * dHt(1, 0) + Ht.y() * dHt(1, 1);
        F += toCartesian(Fcyl, ur);
    // }

    double H(std::sqrt(HtCart.dot(HtCart)));
    dlib::vector<double, 3>  force(_mu * _V * _Mp / H * F);
    // std::cout<<force.x()<<" "<<force.z()<<" "<<std::endl;
    return force;
}

dlib::vector<double, 2> MagneticField::cylField(const double R, const dlib::vector<double, 3> &position)
{
    _R = R;
    _r = std::sqrt(position.x() * position.x() + position.y() * position.y());
    _z = position.z();
    double Hr;
    double Hz;
    if (_r >= _tol)
    {
        Hr = _Ms / (2 * dlib::pi) * dlib::integrate_function_adapt_simp(&MagneticField::Pir, -_h, 0.0, _tol);
    }

    Hz = _Ms / (2 * dlib::pi) * dlib::integrate_function_adapt_simp(&MagneticField::Piz, -_h, 0.0, _tol);

    dlib::vector<double, 2> Ht(Hr, Hz);

    return Ht;
}

dlib::vector<double, 3> MagneticField::toCartesian(const dlib::vector<double, 2> &vCyl, dlib::vector<double, 2> &ur) const
{
    dlib::vector<double, 3> vCart(vCyl.x() * ur.x(), vCyl.x() * ur.y(), vCyl.y());
    return vCart;
}

dlib::matrix<double, 2, 2> MagneticField::cylFieldGradient(const double R, const dlib::vector<double, 3> &position)
{
    _R = R;
    _r = std::sqrt(position.x() * position.x() + position.y() * position.y());
    _z = position.z();
    double dHrr;
    double dHrz;
    double dHzr;
    double dHzz;
    if (_r >= _tol)
    {
        dHrr = _Ms / (2 * dlib::pi) * dlib::integrate_function_adapt_simp(&MagneticField::Pirr, -_h, 0.0, _tol);
        if (_dz != 0.)
            dHrz = _Ms / (2 * dlib::pi) * dlib::integrate_function_adapt_simp(&MagneticField::Pirz, -_h, 0.0, _tol);
    }

    dHzr = _Ms / (2 * dlib::pi) * dlib::integrate_function_adapt_simp(&MagneticField::Pizr, -_h, 0.0, _tol);
    dHzz = _Ms / (2 * dlib::pi) * dlib::integrate_function_adapt_simp(&MagneticField::Pizz, -_h, 0.0, _tol);

    dlib::matrix<double, 2, 2> dHt;
    dHt = dHrr, dHrz, dHzr, dHzz;

    return dHt * 1e6;
}

double MagneticField::Pirr(const double zp)
{
    _dz = dz(zp);
    _f = f();
    _d = d();
    _Rt = Rt();
    _k = k();
    _E = E();
    _K = K();
    _dRtr = dRtr();
    _dkr = dkr();
    _dE = dE();
    _dK = dK();

    return -(1. / _r + (_R + _r) * _f * _f) * Pir(zp) + _dz * _f / _r * (_dRtr * _E + _dkr * (_Rt * _dE - _dK));
}

double MagneticField::Pirz(const double zp)
{
    _dz = dz(zp);
    _f = f();
    _d = d();
    _Rt = Rt();
    _k = k();
    _E = E();
    _K = K();
    _dRtz = dRtz();
    _dkz = dkr();
    _dE = dE();
    _dK = dK();

    return (_R + _r) * _f * _f / _dz * Pir(zp) + _dz * _f / _r * (_dRtz * _E + _dkz * (_Rt * _dE - _dK));
}

double MagneticField::Pizr(const double zp)
{
    _dz = dz(zp);
    _f = f();
    _d = d();
    _Zt = Zt();
    _k = k();
    _E = E();
    _K = K();
    _dZtr = dZtr();
    _dkr = dkr();
    _dE = dE();
    _dK = dK();

    return -(_R + _r) * _f * _f * Piz(zp) + _f * (_dZtr * _E + _dkr * (_Zt * _dE + _dK));
}

double MagneticField::Pizz(const double zp)
{
    _dz = dz(zp);
    _f = f();
    _d = d();
    _Zt = Zt();
    _k = k();
    _E = E();
    _K = K();
    _dZtz = dZtz();
    _dkz = dkz();
    _dE = dE();
    _dK = dK();

    return -_dz * _f * _f * Piz(zp) + _f * (_dZtz * _E + _dkz * (_Zt * _dE + _dK));
}

double MagneticField::Pir(const double zp)
{
    _dz = dz(zp);
    _f = f();
    _d = d();
    _Rt = Rt();
    _k = k();
    _E = E();
    _K = K();

    return _dz / _r * _f * (_Rt * _E - _K);
}

double MagneticField::Piz(const double zp)
{
    _dz = dz(zp);
    _f = f();
    _d = d();
    _Zt = Zt();
    _k = k();
    _E = E();
    _K = K();

    return _f * (_Zt * _E + _K);
}

double MagneticField::fk(const double phi)
{
    return 1. / std::sqrt(1 - _k * _k * std::sin(phi) * std::sin(phi));
}

double MagneticField::dfk(const double phi)
{
    return _k * std::sin(phi) * std::sin(phi) * std::pow(1 - _k * _k * std::sin(phi) * std::sin(phi), -3. / 2.);
}

double MagneticField::K()
{
    double integral(1.);
    if (_k != 0.)
        integral = dlib::integrate_function_adapt_simp(&MagneticField::fk, 0.0, dlib::pi / 2., _tol);
    return integral;
}

double MagneticField::dK()
{
    double integral(0.);
    if (_k != 0.)
        integral = dlib::integrate_function_adapt_simp(&MagneticField::dfk, 0.0, dlib::pi / 2., _tol);
    return integral;
}

double MagneticField::fe(const double phi)
{
    return std::sqrt(1 - _k * _k * std::sin(phi) * std::sin(phi));
}

double MagneticField::dfe(const double phi)
{
    return -_k * std::sin(phi) * std::sin(phi) / std::sqrt(1 - _k * _k * std::sin(phi) * std::sin(phi));
}

double MagneticField::E()
{
    double integral(1.);
    if (_k != 0.)
        integral = dlib::integrate_function_adapt_simp(&MagneticField::fe, 0.0, dlib::pi / 2., _tol);
    return integral;
}

double MagneticField::dE()
{
    double integral(0.);
    if (_k != 0.)
        integral = dlib::integrate_function_adapt_simp(&MagneticField::dfe, 0.0, dlib::pi / 2., _tol);
    return integral;
}

double MagneticField::d()
{
    return (_R - _r) * (_R - _r) + _dz * _dz;
}

double MagneticField::f()
{
    return 1.0 / std::sqrt((_R + _r) * (_R + _r) + _dz * _dz);
}

double MagneticField::k()
{
    return std::sqrt(4 * _r * _R / ((_R + _r) * (_R + _r) + _dz * _dz));
}

double MagneticField::dkr()
{
    double d(0.);
    if (_k != 0.)
        d = (2 * _k * _R - _k * _k * _k * (_R + _r)) / (4 * _r * _R);
    return d;
}

double MagneticField::dkz()
{
    double d(0.);
    if (_k != 0.)
        d = -(_k * _k * _k * _dz) / (4 * _r * _R);
    return d;
}

double MagneticField::Rt()
{
    return (_R * _R + _r * _r + _dz * _dz) / _d;
}

double MagneticField::dRtr()
{
    return 2. * (_r + _Rt * (_R - _r)) / _d;
}

double MagneticField::dRtz()
{
    return 2. * (_dz - _Rt * _dz) / _d;
}

double MagneticField::Zt()
{
    return (_R * _R - _r * _r - _dz * _dz) / _d;
}

double MagneticField::dZtr()
{
    return 2. * (-_r + _Zt * (_R - _r)) / _d;
}

double MagneticField::dZtz()
{
    return 2. * (-_dz - _Zt * _dz) / _d;
}

double MagneticField::dz(const double zp)
{
    return _z - zp;
}

MagneticField::~MagneticField()
{
}

double MagneticField::_R = 0.;
double MagneticField::_r = 0.;
double MagneticField::_z = 0.;
double MagneticField::_dz = 0.;
double MagneticField::_f = 0.;
double MagneticField::_d = 0.;
double MagneticField::_Rt = 0.;
double MagneticField::_Zt = 0.;
double MagneticField::_dRtr = 0.;
double MagneticField::_dZtr = 0.;
double MagneticField::_dRtz = 0.;
double MagneticField::_dZtz = 0.;
double MagneticField::_dkr = 0.;
double MagneticField::_dkz = 0.;
double MagneticField::_dE = 0.;
double MagneticField::_dK = 0.;
double MagneticField::_k = 0.;
double MagneticField::_E = 0.;
double MagneticField::_K = 0.;
//double MagneticField::_tol = 1e-8;
double MagneticField::_tol = 1e-6;
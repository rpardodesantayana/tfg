#include "VanDerWaals.h"
#include <cmath>
#include <dlib/numerical_integration.h>
#include <dlib/numeric_constants.h>

VanDerWaals::VanDerWaals(const double Rp,
                         const double A)
    : _Rp(Rp),
      _A(A)
{
}

dlib::vector<double, 3> VanDerWaals::force(const dlib::vector<double, 3> &relPosition) const
{
    const dlib::vector<double, 3> pos = relPosition* 1e-6;
    double r = std::sqrt(pos.dot(pos)) ;
    double d = 2. * _Rp * 1e-6;
    double h = r - d;

    double Fij(_A/6.*std::pow(d,6.)/std::pow(h*h+2.*d*h,2.)/std::pow(h+d,3.));

    if (h<=0.)return 0*pos;

    return Fij*pos/r;

}

VanDerWaals::~VanDerWaals()
{
}

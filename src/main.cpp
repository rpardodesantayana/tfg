#include "MagneticField.h"
#include "Hydro.h"
#include "Surfactant.h"
#include "VanDerWaals.h"
#include <algorithm>
#include <dlib/geometry.h>
#include <dlib/matrix.h>
#include <dlib/numerical_integration.h>
#include <sstream>
#include <fstream>

int main(int argc, char *argv[])
{
    // // Parse inputs
    // if (argc < 2)
    // {
    //     std::cout << "You must provide at least one argument" << std::endl;
    //     return 1;
    // }
    // std::istringstream ss(argv[1]);
    // int nP;
    // if (!(ss >> nP))
    // {
    //     std::cerr << "Invalid number: " << argv[1] << '\n';
    // }
    // else if (!ss.eof())
    // {
    //     std::cerr << "Trailing characters after number: " << argv[1] << '\n';
    // }

    // Parse inputs
    size_t maxStep;
    double maxTime;
    int nP;
    double Rout;
    double Rin;
    double h;
    double Hbias;
    double T;
    double rc;
    double rp;

    std::ifstream inFile;
    inFile.open("in.dat");
    inFile >> maxStep;
    inFile >> maxTime;
    inFile >> nP;
    inFile >> Rout;
    inFile >> Rin;
    inFile >> h;
    inFile >> Hbias;
    inFile >> T;
    inFile >> rc;
    inFile >> rp;
    inFile.close();

    // Open output files
    std::ofstream outFile;
    outFile.open("out.dat");
    outFile << "#  Time[s] X[mum] Y[mum] Z[mum] ..." << std::endl;

    std::ofstream outFile2;
    outFile2.open("out2.dat");
    outFile2 << "#  Time[s] dt[s] Ec[J] Dp[mum] ..." << std::endl;

    // Parameters
    double Ms = 8.6e5;
    double Mp = 4.78e5;
    double muf = 1.256627e-6; //[N/A^2]
    double rs = rc;
    double Vc = 3. / 4. * dlib::pi * std::pow(rc * 1e-6, 3.);
    double Vp = 3. / 4. * dlib::pi * std::pow(rp * 1e-6, 3.);
    double lx = 2.;
    double ly = 2.;
    double lz = 1.;
    double d = 2 * rp;                              //< particle diameter [mu m]
    double nu = 0.001;                              //< viscosoty [N.s/m^2]
    double kb = 1.380649e-23;                       //< Boltzmann constant [J⋅K−1]
    double Na = 6.02214086e23;                      //< Avogadro constant
    double Ns = std::pow(2.e6 * Na / 60., 2. / 3.); //< surface density of surfactant molecules [1/m^2]
    double A = 0.63e-20;                            //< Hamaker constant [J]
    double roc = 5000;                              //< ro core
    double ros = 2648;                              //< ro shell
    double m = Vc * roc + (Vp - Vc) * ros;          //< particle mass [kg]

    // Initialise forces
    MagneticField magField(Rout, Rin, lx, ly, h, Hbias, Ms, Mp, muf, Vc);
    Hydro hydro(d, muf, nu, kb, T);
    Surfactant surf(rp, Ns, kb, T, rs);
    VanDerWaals vdw(rp, A);

    // int nx(1000);
    // dlib::vector<double, 3> pp;
    // dlib::vector<double, 3> force2;
    // pp(1) = 0.;
    // pp(2) = rp;

    // for (size_t i = 0; i < nx + 1; i++)
    // {
    //     pp(0) = -lx / 2. + lx * i / nx;
    //     std::cout<<pp(0)<<" ";
    //     force2 = magField.magneticForce(pp);
    // }


    // return 0;

    double dSqDipole = std::pow(Rin, 2);
    double dSqWdv = std::pow(1.5 * d, 2);
    double dSqSurf = std::pow(1.5 * d, 2);
    double dSqVel = std::pow(1.5 * d, 2);
    double dSqMax = std::max(std::max(dSqDipole, dSqWdv), std::max(dSqSurf, dSqVel));
    double dSqMin = std::min(std::min(dSqDipole, dSqWdv), std::min(dSqSurf, dSqVel));
    double dContactSq2 = std::pow(2. * rp + 0.e-3, 2);
    double dContactSq = std::pow(2. * rp + 1.0e-3, 2);
    double dfMagSq;
    double dfDipSq;
    double dfHydSq;
    double dfSurSq;
    double dfVdwSq;

    // Init particle state
    const double fDt = 2.;
    dlib::matrix<double> minDistSq(nP, 1);
    dlib::matrix<double> fSq(nP, 1);
    dlib::matrix<double> vSq(nP, 1);
    dlib::matrix<double> p(nP, 3);
    dlib::matrix<double> dp(nP, 3);
    dlib::matrix<double> v(nP, 3);
    dlib::matrix<double> f(nP, 3);
    dlib::matrix<double> pB(nP, 3);
    dlib::matrix<double> magMom(nP, 3);
    std::default_random_engine gen;
    std::uniform_real_distribution<double> dx(-lx / 2., lx / 2.);
    std::uniform_real_distribution<double> dy(-ly / 2., ly / 2.);
    std::uniform_real_distribution<double> dz(0, lz);
    for (size_t i = 0; i < nP; i++)
    {
        p(i, 0) = dx(gen);
        p(i, 1) = dy(gen);
        p(i, 2) = dz(gen);
    }

    // Make sure they are spread
    for (size_t i = 0; i < nP; i++)
    {
        // Get position
        dlib::vector<double, 3> pi = rowm(p, i);
        // Loop over rest of particles
        for (size_t j = i + 1; j < nP; j++)
        {
            // Get position
            dlib::vector<double, 3> pj = rowm(p, j);

            // Relative position
            dlib::vector<double, 3> relPosition = pj - pi;

            // Compute distance squared
            double dSq = relPosition.dot(relPosition);

            // If a PN is penetrated expell
            while (dSq <= dContactSq)
            {
                pj += relPosition / 10.;
                // Set partner interparticle force
                set_rowm(p, j) = trans(pj);
                relPosition = pj - pi;
                dSq = relPosition.dot(relPosition);
            }
        }
    }

    // outFile << "init positions: " << std::endl;
    // outFile << p << std::endl;

    const double D = hydro.getDrag();
    const double dtMax = 1.e5 * m / D;
    const double dtMin = 1.e-10;
    double dt = dtMax;
    double time(0.);
    double Ke(0.);
    dlib::vector<double, 3> avDp = {0., 0., 0.};
    double avZ = 0.;
    int cnt = 0;
    int cnt2 = 0;
    dlib::vector<double, 3> force;
    // outFile << "dt: " << dt << std::endl;

    // One step
    for (size_t step = 0; step < maxStep; step++)
    {
        if (time > maxTime)
            break;
        //outFile << "step: " << step << std::endl;

        // Reset Forces
        dfMagSq = 0.;
        dfDipSq = 0.;
        dfHydSq = 0.;
        dfSurSq = 0.;
        dfVdwSq = 0.;

        f = dlib::zeros_matrix<double>(nP, 3);

        // Reset min distance
        minDistSq = std::sqrt(lx * lx + ly * ly + lz * lz) * dlib::ones_matrix<double>(nP, 1);

        // Loop over particles
        for (size_t i = 0; i < nP; i++)
            // Compute magnetic moment
            set_rowm(magMom, i) = trans(magField.effMagMoment(rowm(p, i)));

        // Loop over particles
        for (size_t i = 0; i < nP; i++)
        {
            // Compute particle forces
            dlib::vector<double, 3> fi = rowm(f, i);

            // Get position
            dlib::vector<double, 3> pi = rowm(p, i);

            // Compute Magnetic Force
            force = magField.magneticForce(pi);
            if (force.dot(force) > dfMagSq)
                dfMagSq = force.dot(force);
            fi += force;

            // Get particle magnetic moment
            dlib::vector<double, 3> mi = rowm(magMom, i);

            // Loop over rest of particles
            for (size_t j = i + 1; j < nP; j++)
            {

                // Get position
                dlib::vector<double, 3> pj = rowm(p, j);

                // Relative position
                dlib::vector<double, 3> relPosition = pj - pi;

                // Compute distance squared
                double dSq = relPosition.dot(relPosition);

                // If a PN is penetrated expell
                // while (dSq <= dContactSq)
                // {
                //     pj += relPosition / 10.;
                //     // Set partner interparticle position
                //     set_rowm(p, j) = trans(pj);
                //     relPosition = pj - pi;
                //     dSq = relPosition.dot(relPosition);
                // }
                // If a PN is penetrated compute elastic collision
                if (dSq <= dContactSq)
                {
                    relPosition = pj - pi;
                    dlib::vector<double, 3> n = relPosition / std::sqrt(relPosition.dot(relPosition));
                    dlib::vector<double, 3> v1 = rowm(v, i);
                    dlib::vector<double, 3> v2 = rowm(v, j);
                    double u1 = v1.dot(n);
                    double u2 = v2.dot(n);
                    double dp = std::sqrt(dContactSq) - std::sqrt(dSq);
                    // Coumpute change in state
                    pi -= dp * n;
                    pj += dp * n;
                    v1 += (u2 - u1) * n;
                    v2 += (u1 - u2) * n;
                    // Set positions and valocities
                    set_rowm(p, i) = trans(pi);
                    set_rowm(p, j) = trans(pj);
                    set_rowm(v, i) = trans(v1);
                    set_rowm(v, j) = trans(v2);
                    relPosition = pj - pi;
                    dSq = relPosition.dot(relPosition);
                }

                if (dSq < minDistSq(i, 0))
                    minDistSq(i, 0) = dSq;
                if (dSq < minDistSq(j, 0))
                    minDistSq(j, 0) = dSq;

                // Interparticle force
                dlib::vector<double, 3> fij;

                // If within influence sphere compute interparticle forces
                if (dSq <= dSqDipole)
                {
                    // Get particle magnetic moment
                    dlib::vector<double, 3> mj = rowm(magMom, j);

                    // Dipole-dipole force
                    force = magField.dipoleForce(mi, mj, relPosition);
                    if (force.dot(force) > dfDipSq)
                        dfDipSq = force.dot(force);
                    fij += force;
                }

                // If within influence sphere compute interparticle forces
                if (dSq <= dSqSurf)
                {
                    // Surfactant force
                    force = surf.force2(relPosition);
                    if (force.dot(force) > dfSurSq)
                        dfSurSq = force.dot(force);

                    fij += force;
                }

                // If within influence sphere compute interparticle forces
                if (dSq <= dSqWdv)
                {
                    // Van der Waals force
                    force = vdw.force(relPosition);
                    if (force.dot(force) > dfVdwSq)
                        dfVdwSq = force.dot(force);
                    fij += force;
                }

                // If within influence sphere compute interparticle forces
                if (dSq <= dSqVel)
                {
                    // Relative velocity
                    dlib::vector<double, 3> relVelocity = rowm(v, j) - rowm(v, i);

                    // Hydrodynamic interaction
                    force = hydro.lubricantForce(relPosition, relVelocity);
                    if (force.dot(force) > dfHydSq)
                        dfHydSq = force.dot(force);
                    fij += force;
                }

                if (dSq <= dSqMax)
                {
                    // Set particle interparticle force
                    fi += fij;

                    // Compute particle forces
                    dlib::vector<double, 3> fj = rowm(f, j);

                    // Set partner particle interparticle force
                    fj -= fij;

                    // Set partner interparticle force
                    set_rowm(f, j) = trans(fj);
                }
            }

            // Set particle forces
            set_rowm(f, i) = trans(fi);
        }

        for (size_t i = 0; i < nP; i++)
        {

            // Compute particle forces
            dlib::vector<double, 3> fi = rowm(f, i);

            // Get position
            dlib::vector<double, 3> pi = rowm(p, i);

            // Compensate particle forces and velocities on z boundaries
            if (pi.z() <= rp)
                fi.z() = std::max(fi.z(), 0.);
            if (pi.z() >= lz - rp)
                fi.z() = std::min(fi.z(), 0.);

            // Set particle forces
            set_rowm(f, i) = trans(fi);

            // Store force squared
            fSq(i, 0) = fi.dot(fi);
        }

        // Compute delta time
        dlib::matrix<double> dtM(nP, 1);
        for (size_t i = 0; i < nP; i++)
        {
            dlib::vector<double, 3> vi = rowm(v, i);
            vSq(i, 0) = vi.dot(vi);
            dtM(i, 0) = dtMax;
            if (std::sqrt(minDistSq(i, 0)) > 0. && std::sqrt(fSq(i, 0)) > 0. && vSq(i, 0) > 0.)
            {
                double dmin = std::min(rp, std::sqrt(minDistSq(i, 0)) - std::sqrt(dContactSq2)) / fDt * 1.e-6;
                dtM(i, 0) = std::min(
                    D * dmin / std::sqrt(fSq(i, 0)),
                    dmin / std::sqrt(vSq(i, 0)));
            }
        }
        std::sort(dtM.begin(), dtM.end());
        dt = dtM(0, 0);
        dt = std::max(dtMin, std::min(dt, dtMax));

        // Loop over particles
        for (size_t i = 0; i < nP; i++)
            // Brownian motion
            // if (minDistSq(i, 0) > dSqMin)
            // {
            set_rowm(pB, i) = trans(hydro.brownianDiffusion(dt));
        // }
        // else
        // {
        //     set_rowm(pB, i) = dlib::zeros_matrix<double>(1, 3);
        // }

        // Propagation
        dp = (f * dt / D + m / D * (v - f / D) * (1. - std::exp(-D * dt / m)) + pB) * 1e6;
        p = p + dp;
        v = f * dt / D + (v - f / D) * std::exp(-D * dt / m);
        time += dt;

        // Compute kinetic energy
        Ke = 0.;
        for (size_t i = 0; i < nP; i++)
            Ke += vSq(i, 0);
        Ke *= 0.5 * m;

        // Compute average dp
        avDp = {0., 0., 0.};
        for (size_t i = 0; i < nP; i++)
        {
            dlib::vector<double, 3> vDp(rowm(dp, i));
            avDp += vDp;
        }
        avDp /= nP;

        // Compute average z
        avZ = 0.;
        for (size_t i = 0; i < nP; i++)
            avZ += p(i, 2);
        avZ /= nP;

        if (avZ <= rp)
            cnt++;

        if (cnt > 1000)
            break;

        bool flag = true;
        for (size_t i = 0; i < nP; i++)
            if (p(i, 2) > 2.2 * rp)
            {
                flag = false;
                break;
            }
        if (flag)
            cnt2++;

        if (cnt2 > 1000)
            break;

        // Keep particles in the box
        dlib::matrix<double> px = colm(p, 0);
        dlib::matrix<double> py = colm(p, 1);
        dlib::matrix<double> pz = colm(p, 2);

        // // Periodic XY boundaries
        px = px - lx * round(px / lx);
        py = py - ly * round(py / ly);

        // Non periodic Z boundary
        for (size_t i = 0; i < nP; i++)
        {
            pz(i, 0) = std::max(pz(i, 0), rp);
            pz(i, 0) = std::min(pz(i, 0), lz - rp);
        }

        set_colm(p, 0) = px;
        set_colm(p, 1) = py;
        set_colm(p, 2) = pz;

        //Print all particles
        if (step % 10 == 0)
        {
            outFile << time;
            for (size_t i = 0; i < nP; i++)
                outFile << " " << p(i, 0) << " " << p(i, 1) << " " << p(i, 2);
            outFile << std::endl;

            outFile2 << time << " " << dt << " " << Ke << " " << avDp.x() << " " << avDp.y() << " " << avDp.z() << " " << avZ;
            outFile2 << " " << std::sqrt(dfMagSq) << " " << std::sqrt(dfDipSq) << " " << std::sqrt(dfSurSq);
            outFile2 << " " << std::sqrt(dfVdwSq) << " " << std::sqrt(dfHydSq) << std::endl;
        }
    }

    outFile << time;
    for (size_t i = 0; i < nP; i++)
        outFile << " " << p(i, 0) << " " << p(i, 1) << " " << p(i, 2);
    outFile << std::endl;
    outFile2 << time << " " << dt << " " << Ke << " " << avDp.x() << " " << avDp.y() << " " << avDp.z() << " " << avZ;
    outFile2 << " " << std::sqrt(dfMagSq) << " " << std::sqrt(dfDipSq) << " " << std::sqrt(dfSurSq);
    outFile2 << " " << std::sqrt(dfVdwSq) << " " << std::sqrt(dfHydSq) << std::endl;
    //outFile << p;

    // Close files
    outFile.close();
    outFile2.close();

    return 0;
}

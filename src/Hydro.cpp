#include "Hydro.h"
#include <cmath>
#include <dlib/numerical_integration.h>
#include <dlib/numeric_constants.h>

Hydro::Hydro(const double d,
             const double mu,
             const double nu,
             const double kb,
             const double T)
    : _d(d),
      _D(3. * dlib::pi * nu * _d * 1e-6),
      _c(3. / 8. * dlib::pi * mu * _d * _d * 1.e-12),
      _bf(std::sqrt(2. * kb * T / _D)),
      _gen(),                   // seed
      _dis(0.0, 1.0),           // random magnitude
      _costheta(-1., 1.),       // random costheta
      _phi(-dlib::pi, dlib::pi) // random phi

{
}

dlib::vector<double, 3> Hydro::lubricantForce(const dlib::vector<double, 3> &relPosition,
                                              const dlib::vector<double, 3> &relVelocity) const
{

    double r = std::sqrt(relPosition.dot(relPosition));
    double h = r - _d;
    if (h <= 0.)
        return 0. * relVelocity;
    return _c * relVelocity / h / 1e-6;
}

dlib::vector<double, 3> Hydro::brownianDiffusion(const double dt)
{

    double costheta = _costheta(_gen);
    double theta = std::acos(costheta);
    double phi = _phi(_gen);
    dlib::vector<double, 3> n(std::sin(theta) * std::cos(phi),
                              std::sin(theta) * std::sin(phi),
                              std::cos(theta));

    return n * _dis(_gen) * _bf * std::sqrt(dt);
}

double Hydro::getDrag() const
{
    return _D;
}

Hydro::~Hydro()
{
}